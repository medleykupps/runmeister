package com.pedrera.runmeister;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.pedrera.runmeister.models.Run;
import com.pedrera.runmeister.models.RunningLocation;

public class RunMapActivity extends ActionBarActivity {

    private GoogleMap _map;
    private Run _run;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run_map);

        Intent intent = getIntent();
        if (intent != null) {
            _run = (Run) intent.getSerializableExtra("run");
        }

        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #_map} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (_map == null) {
            // Try to obtain the map from the SupportMapFragment.
            _map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (_map != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #_map} is not null.
     */
    private void setUpMap() {

        if (_map == null || _run == null) {
            return;
        }

        PolylineOptions polylineOptions = new PolylineOptions();
        for (int i = 0; i < _run.get_locations().size(); i++) {
            RunningLocation location = _run.get_locations().get(i);
            // _map.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("Marker"));
            polylineOptions.add(new LatLng(location.getLatitude(), location.getLongitude()));
        }

        _map.addPolyline(polylineOptions);
        centreMap();
    }

    private void centreMap() {
        if (_map == null || _run == null) {
            return;
        }
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (int i = 0; i < _run.get_locations().size(); i++) {
            RunningLocation location = _run.get_locations().get(i);
            builder.include(new LatLng(location.getLatitude(), location.getLongitude()));
        }
        LatLngBounds bounds = builder.build();
        // CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 4);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 100, 100, 4);
        _map.moveCamera(cameraUpdate);
    }
}
