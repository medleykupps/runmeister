package com.pedrera.runmeister.models;

import org.joda.time.DateTime;

import java.io.Serializable;

public class RunningLocation implements Serializable{

    private final double latitude;
    private final double longitude;
    private final DateTime date;

    private static final long serialVersionUID = 7526471155622776153L;

    public RunningLocation(double latitude, double longitude, DateTime date){

        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public DateTime getDate() {
        return date;
    }
}
