package com.pedrera.runmeister.models;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Run implements Serializable
{
    public enum RunStatus {
        None,
        Created,
        Started,
        Stopped
    }

    private int _id;
    private DateTime _start;
    private DateTime _end;
    private List<RunningLocation> _locations;
    private RunStatus _status;

    private static final long serialVersionUID = 7526471155622776147L;

    public Run() {
        _locations = new ArrayList<RunningLocation>();
        _status = RunStatus.None;
    }

    public Run(DateTime start){
        _start = start;
        _locations = new ArrayList<RunningLocation>();
        _status = RunStatus.Started;
    }

    public DateTime get_start() {
        return _start;
    }

    public void set_start(DateTime _start) {
        this._start = _start;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void clear_locations() {
        _locations.clear();
    }

    public List<RunningLocation> get_locations() {
        return _locations;
    }

    public void add_location(RunningLocation location) {
        _locations.add(location);
    }

    public DateTime get_end() {
        return _end;
    }

    public void set_end(DateTime _end) {
        this._end = _end;
    }
}
