package com.pedrera.runmeister.utils;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class ServiceProxyOperation extends AsyncTask<String, Void, Void> {

    private String mResponseBody;
    private String TAG = "ServiceProxyOperation";

    @Override
    protected Void doInBackground(String... urls) {

        BufferedReader br = null;
        HttpURLConnection connection = null;

        try {

            URL url = new URL(urls[0]);
            connection = (HttpURLConnection) url.openConnection();

//            connection.setDoOutput(true);
//            OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
//            osw.write(responseBody);
//            osw.flush();

            int responseCode = connection.getResponseCode();
            if (responseCode == 200) {
                br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                mResponseBody = sb.toString();

                Log.i(TAG, "Service response: '" + mResponseBody + "'");
            }
            else {
                Log.e("ServiceProxyOperation", "Service call failed with response code " + Integer.toString(responseCode));
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                br.close();
                connection.disconnect();
            }
            catch (Exception e) {
                // do nothing
            }
        }

        return null;
    }
}
