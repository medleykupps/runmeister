package com.pedrera.runmeister;

import android.content.Intent;
import android.location.Location;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationListener;
import com.pedrera.runmeister.models.Run;
import com.pedrera.runmeister.models.RunningLocation;
import com.pedrera.runmeister.services.IRunningService;
import com.pedrera.runmeister.services.RunningLocationService;
import com.pedrera.runmeister.utils.ServiceProxy;

import org.joda.time.DateTime;

import java.io.Serializable;

public class MainActivityFragment extends Fragment
    implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener
{
    public MainActivityFragment() {
    }

    private GoogleApiClient _playClient;
    private IRunningService _runningService;
    private Run _currentRun;

    private ServiceProxy mServiceProxy;
    private static final String TAG = MainActivityFragment.class.getSimpleName();
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    private Location mLastLocation;

    private Run.RunStatus _runStatus = Run.RunStatus.None;

    private LocationRequest mLocationRequest;
    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters
    // UI elements
    private TextView lblLocation;

    private Button btnNewRun;
    private Button btnShowLocation;
    private Button btnStartRun;
    private Button btnStopRun;
    private Button btnPostLocationHistory;
    private Button btnShowRun;

    @Override
    public void onStart(){
        super.onStart();
        if (_playClient != null) {
            _playClient.connect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checkPlayServices();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _runningService = new RunningLocationService();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lblLocation = (TextView) getView().findViewById(R.id.lblLocation);
        btnShowLocation = (Button) getView().findViewById(R.id.btnShowLocation);
        btnStartRun = (Button) getView().findViewById(R.id.btnStartRun);
        // btnPostLocationHistory = (Button) getView().findViewById(R.id.btnPostLocationHistory);
        btnShowRun = (Button) getView().findViewById(R.id.btnShowRun);
        btnNewRun = (Button) getView().findViewById(R.id.btnNewRun);
        btnStopRun = (Button) getView().findViewById(R.id.btnStopRun);

        if (checkPlayServices()) {
            buildGoogleApiClient();
            createLocationRequest();
        }

        mServiceProxy = new ServiceProxy();

        btnShowLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLocation();
            }
        });
        btnStartRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRun();
            }
        });
//        btnPostLocationHistory.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                postLocationHistory();
//            }
//        });
        btnShowRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRun();
            }
        });
        btnNewRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewRun();
            }
        });
        btnStopRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRun();
            }
        });
    }



    private void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(_playClient, mLocationRequest, this);
    }

    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(_playClient, this);
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {
        Location loc = LocationServices.FusedLocationApi.getLastLocation(_playClient);
        displayLocation(loc);
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        _playClient.connect();
    }

    private void displayLocation(Location location) {
        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            lblLocation.setText(latitude + ", " + longitude);
        } else {
            lblLocation.setText("(Couldn't get the location. Make sure location is enabled on the device)");
        }
    }

    protected synchronized void buildGoogleApiClient() {
        _playClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        Log.i(TAG, "isGooglePlayServicesAvailable " + Integer.toString(resultCode));
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(),
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getActivity().getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                getActivity().finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {

        RunningLocation loc = new RunningLocation(location.getLatitude(), location.getLongitude(), DateTime.now());
        _currentRun.add_location(loc);
        mLastLocation = location;
        Toast.makeText(getActivity().getApplicationContext(), "Location changed", Toast.LENGTH_SHORT).show();
        displayLocation(mLastLocation);
    }

    // Commands
    private void createNewRun() {
        if (_currentRun != null || _runStatus != Run.RunStatus.None || _runStatus != Run.RunStatus.Stopped) {
            // Confirm we want to exit the existing Run...which shouldn't happen
        }
        _currentRun = _runningService.createNewRun();
        _runStatus = Run.RunStatus.Created;
        onRunCreated(_currentRun);
    }

    private void startRun() {
        if (_runStatus != Run.RunStatus.Created) {
            Log.w(TAG, "Attempting to start a run in wrong state");
            return;
        }
        _currentRun.set_start(DateTime.now());
        _runStatus = Run.RunStatus.Started;
        startLocationUpdates();

        onRunStarted(_currentRun);
    }

    private void stopRun() {
        if (_runStatus != Run.RunStatus.Started) {
            Log.w(TAG, "Attempting to stop a run in wrong state");
            return;
        }
        _currentRun.set_end(DateTime.now());
        _runStatus = Run.RunStatus.Stopped;
        stopLocationUpdates();

        onRunStopped(_currentRun);
    }

    private void showLocation() {
        Location loc = LocationServices.FusedLocationApi.getLastLocation(_playClient);
        displayLocation(loc);
    }

    private void showRun()
    {
        Intent showRunIntent = new Intent(getActivity(), RunMapActivity.class);
        showRunIntent.putExtra("run", (Serializable) _currentRun);
        startActivity(showRunIntent);
    }

    // Events
    private void onRunCreated(Run run) {
        btnNewRun.setVisibility(View.GONE);
        btnShowRun.setVisibility(View.GONE);
        btnStartRun.setVisibility(View.VISIBLE);
    }

    private void onRunStarted(Run run) {
        btnStartRun.setVisibility(View.GONE);
        btnStopRun.setVisibility(View.VISIBLE);
    }

    private void onRunStopped(Run run) {
        btnStopRun.setVisibility(View.GONE);
        btnShowRun.setVisibility(View.VISIBLE);
        btnNewRun.setVisibility(View.VISIBLE);
    }
}
