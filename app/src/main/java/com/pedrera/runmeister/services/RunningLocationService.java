package com.pedrera.runmeister.services;

import android.util.Log;

import com.pedrera.runmeister.models.Run;
import com.pedrera.runmeister.models.RunningLocation;

import java.util.ArrayList;
import java.util.List;

public class RunningLocationService implements IRunningService {

    public RunningLocationService() {
    }

    @Override
    public Run createNewRun() {
        return new Run();
    }

    @Override
    public boolean saveRun(Run run) {
        return false;
    }

    //    @Override
//    public void updateCurrentLocation(RunningLocation runningLocation) {
//        PostedRunningLocation postedRunningLocation = new PostedRunningLocation(runningLocation);
//        runningLocations.add(postedRunningLocation);
//        if (!postToService(postedRunningLocation)) {
//            Log.e(this.getClass().getSimpleName(), "Error posting location");
//        }
//    }

    private Boolean postToService(PostedRunningLocation postedRunningLocation){
        return false;
    }

    class PostedRunningLocation {
        private RunningLocation runningLocation;
        private Boolean posted;

        public PostedRunningLocation(RunningLocation runningLocation) {
            this.runningLocation = runningLocation;
            this.posted = false;
        }

        public RunningLocation getRunningLocation() {
            return runningLocation;
        }

        public void setRunningLocation(RunningLocation runningLocation) {
            this.runningLocation = runningLocation;
        }

        public Boolean getPosted() {
            return posted;
        }

        public void setPosted(Boolean posted) {
            this.posted = posted;
        }
    }
}
