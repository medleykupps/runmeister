package com.pedrera.runmeister.services;

import com.pedrera.runmeister.models.Run;

public interface IRunningService {
    Run createNewRun();
    boolean saveRun(Run run);
}
